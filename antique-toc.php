<?php
/**
 * Plugin Name: Antique Table of Contents
 * Plugin URI: https://gitlab.com/wp-antique/plugins/antique-toc
 * Description: Add a customizable table of contents to your pages so that visitors obtain an overview of your article.
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: antique-toc
 * Domain Path: /languages
 */
/*
  Copyright 2023 Simon Garbin
  Antique Table of Contents is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Antique Table of Contents is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Antique Table of Contents. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
/*
  Comments:
  - Every function is prefixed by the plugin name in order to prevent
  name collisions.
  - In almost all functions named arguments are used in order to facilitate
  the understanding of the code.
  - Single quotation marks are used for php/js code, double quotation marks
  for HTML.
 */

include plugin_dir_path(__FILE__) . '/shared/menu.php';
include plugin_dir_path(__FILE__) . '/shared/register.php';
include plugin_dir_path(__FILE__) . '/settings.php';

defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_toc_load_textdomain'
);

function antique_toc_load_textdomain() {
    load_plugin_textdomain(
            domain: 'antique-toc',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

function antique_toc_translation_dummy() {
    $plugin_description = __('Add a customizable table of contents to your '
            . 'pages so that visitors obtain an overview of your article.',
            'antique_toc');
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'antique_toc_settings_link'
);

function antique_toc_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=antique_toc');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'antique-toc')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Styles and Scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'wp_head',
        callback: 'antique_toc_custom_style'
);

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_toc_custom_style'
);

function antique_toc_custom_style() {

    $options = get_antique_toc_options();

    $heading_has_border = $options['heading_border_bool'] == 'on';
    $content_has_border = $options['content_border_bool'] == 'on';
    $is_antique = $options['antique_style'] == 'on' && wp_get_theme() == 'Antique';

    $heading_bg_color = $is_antique ? 'var(--antique-blocks--heading--bg-color)' :
            $options['heading_bg_color'];
    $heading_font_color = $is_antique ? 'var(--antique-blocks--heading--font-color)' :
            $options['heading_font_color'];
    $heading_border_color = $is_antique ? 'var(--antique-blocks--heading--border-color)' :
            $options['heading_border_color'];
    $heading_border = $heading_has_border ? sprintf(
                    '%spx %s %s',
                    $options['heading_border_width'],
                    $options['heading_border_style'],
                    $heading_border_color
            ) : 'none';

    $content_bg_color = $is_antique ? 'var(--antique-blocks--content--bg-color)' :
            $options['content_bg_color'];
    $content_font_color = $is_antique ? 'var(--antique-blocks--content--font-color)' :
            $options['content_font_color'];
    $content_border_color = $is_antique ? 'var(--antique-blocks--content--border-color)' :
            $options['content_border_color'];
    $content_border = $content_has_border ? sprintf(
                    '%spx %s %s',
                    $options['content_border_width'],
                    $options['content_border_style'],
                    $content_border_color
            ) : 'none';

    $style = sprintf(
            '<style id="antique-section-search-custom-css">'
            . ':root {'
            . '--antique-toc--heading--bg-color: %s;'
            . '--antique-toc--heading--font-color: %s;'
            . '--antique-toc--heading--border: %s;'
            . '--antique-toc--content--bg-color: %s;'
            . '--antique-toc--content--font-color: %s;'
            . '--antique-toc--content--border: %s;'
            . '}'
            . '</style>',
            $heading_bg_color,
            $heading_font_color,
            $heading_border,
            $content_bg_color,
            $content_font_color,
            $content_border
    );

    echo $style;
}

/*
  ----------------------------------------
  Table of Contents
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_toc_register_block'
);

function antique_toc_register_block() {

    $asset_file = include( plugin_dir_path(__FILE__) . '/block/block.asset.php');
    $script_handle = 'antique-toc-block-script';

    wp_register_script(
            handle: $script_handle,
            src: plugins_url('block/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'antique-toc',
            path: plugin_dir_path(__FILE__) . '/languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/block/',
            args: array(
                'api_version' => 2,
                'render_callback' => 'antique_toc_block_render_callback'
            )
    );
}

function antique_toc_block_render_callback($block_attributes, $content) {
    return get_the_antique_toc();
}

function get_the_antique_toc() {

    $options = get_antique_toc_options();
    $is_closed = $options['on_large_screen'] == 'closed';

    $content = antique_toc_add_header_ids(
            content: get_the_content(get_the_ID())
    );

    $options = get_antique_toc_options();
    $headings_tags = array();

    for ($i = 1; $i <= 6; $i++) {
        $heading_bool = $options['headings_h' . strval($i)];
        if ($heading_bool == 'on') {
            array_push($headings_tags, strval($i));
        }
    }

    $headings = antique_toc_get_headings(
            content: $content,
            tags: $headings_tags
    );

    $toc = '<div class="antique-toc antique-block">'
            . '<div class="antique-toc-heading-wrap antique-block-heading-wrap">'
            . '<span class="antique-toc-heading antique-block-heading">'
            . esc_html__('Table of contents', 'antique-toc')
            . '</span>'
            . '<span class="antique-block-arrow antique-block-show-content'
            . ($is_closed ? ' rotate-arrow' : '')
            . '">'
            . '<i class="fa fa-angle-down"></i>'
            . '</span>'
            . '</div>'
            . '<div class="antique-toc-headings-table-wrap antique-block-content'
            . ($is_closed ? ' hide-content' : '')
            . '">';
    $toc .= antique_toc_create_table($headings);
    $toc .= '     </div>'
            . ' </div>';

    return $toc;
}

function antique_toc_create_table($headings) {
    $toc = '';

    for ($index = 0; $index <= count($headings); $index++) {

        $last_element = $index > 0 ? $headings[$index - 1] : NULL;
        $current_element = isset($headings[$index]) ? $headings[$index] : NULL;
        $next_element = NULL;

        if ($index < count($headings) && isset($headings[$index + 1])) {
            $next_element = $headings[$index + 1];
        }

        if ($current_element == NULL) {
            $toc .= '</li>';
            $toc .= '</ol>';
            break;
        }

        $tag = intval($headings[$index]['tag']);
        $id = $headings[$index]['id'];
        $name = $headings[$index]['name'];

        if ($last_element == NULL) {
            $toc .= '<ol>';
        }
        if ($last_element != NULL && $last_element['tag'] < $tag) {
            for ($i = 0; $i < $tag - $last_element['tag']; $i++) {
                $toc .= '<ol>';
            }
        }

        $toc .= '<li>';
        $toc .= '<a href="#' . $id . '">' . $name . '</a>';

        if ($next_element && intval($next_element['tag']) > $tag) {
            continue;
        }

        $toc .= '</li>';

        if ($next_element && intval($next_element['tag']) == $tag) {
            continue;
        }

        if ($next_element == NULL || ($next_element && $next_element['tag'] < $tag)) {
            $toc .= '</ol>';
            if ($next_element && $tag - intval($next_element['tag']) >= 2) {
                $toc .= '</li>';
                for ($i = 1; $i < $tag - intval($next_element['tag']); $i++) {
                    $toc .= '</ol>';
                }
            }
        }

        if ($next_element != NULL && $next_element['tag'] < $tag) {
            continue;
        }
    }

    return $toc;
}

function antique_toc_get_headings($content, $tags) {

    // $matches = array();

    $regex_tags = implode('|', $tags);

    $opening_tags = '/<h(' . $regex_tags . ')([^<]*)>';
    $closing_tags = '<\/h(' . $regex_tags . ')>/';
    $pattern = $opening_tags . '(.*)' . $closing_tags;
    preg_match_all(
            pattern: $pattern,
            subject: $content,
            matches: $matches,
    );

    $headings = array();

    for ($i = 0; $i < count($matches[1]); $i++) {

        $id_matches = array();
        $class_matches = array();

        $headings[$i]['tag'] = $matches[1][$i];
        $attributes = $matches[2][$i];

        $id_pattern = '/id=\"([^\"]*)\"/';
        preg_match(
                pattern: $id_pattern,
                subject: $attributes,
                matches: $id_matches,
        );
        $headings[$i]['id'] = $id_matches[1];

        $class_pattern = '/class=\"([^\"]*)\"/';
        preg_match_all(
                pattern: $class_pattern,
                subject: $attributes,
                matches: $class_matches
        );
        for ($j = 0; $j < count($class_matches[1]); $j++) {
            $headings[$i]['classes'] = explode(' ', $class_matches[1][$j]);
        }

        $links_pattern = '/<a(?:[^>]+)?>.*?<\/a>/s';
        $headings[$i]['name'] = strip_tags(
                preg_replace(
                        pattern: $links_pattern,
                        replacement: '',
                        subject: $matches[3][$i]
                )
        );
    }

    return $headings;
}

/*
  ----------------------------------------
  Header IDs
  ----------------------------------------
 */

add_filter(
        hook_name: 'the_content',
        callback: 'antique_toc_add_header_ids'
);

function antique_toc_add_header_ids($content) {

    $pattern = '/<h([1-6])([^<]*)>(.*)<\/h[1-6]>/';

    $new_content = preg_replace_callback(
            pattern: $pattern,
            callback: 'antique_toc_add_header_ids_cb',
            subject: $content
    );

    return $new_content;
}

function antique_toc_add_header_ids_cb($matches) {

    // $id_match = array();
    // $classes_match = array();

    $tag = $matches[1];
    $attributes = $matches[2];
    $title = $matches[3];
    $slug = sanitize_title_with_dashes(
            title: antique_toc_replace_umlaute(text: $title)
    );

    $id_pattern = '/id=\"([^\"]*)\"/';
    preg_match(
            pattern: $id_pattern,
            subject: $attributes,
            matches: $id_match,
    );
    $id = isset($id_match[1][0]) ? $id_match[1][0] : '';

    $classes_pattern = '/class=\"([^\"]*)\"/';
    preg_match_all(
            pattern: $classes_pattern,
            subject: $attributes,
            matches: $classes_match
    );
    $classes = isset($classes_match[1][0]) ? $classes_match[1][0] : '';

    if ($id == '') {
        $new_header = '<h' . $tag . ' id="' . $slug . '"';
    } else {
        $new_header = '<h' . $tag . ' id="' . $id . '"';
    }

    if ($classes == '') {
        $new_header .= '>' . $title . '</h' . $tag . '>';
    } else {
        $new_header .= ' class="' . $classes . '">' . $title . '</h' . $tag . '>';
    }

    return $new_header;
}

function antique_toc_replace_umlaute($text) {

    $new_string = str_replace(
            search: array('Ä', 'ä', 'Ö', 'ö', 'Ü', 'ü'),
            replace: array('Ae', 'ae', 'Oe', 'oe', 'Ue', 'ue'),
            subject: $text
    );

    return $new_string;
}
