jQuery(document).ready(function ($) {

    var display = antique_toc_content_display;

    if ($(window).width() <= 768) {
        if (display['on-small-screen'] === 'closed') {
            $('.antique-toc .antique-block-arrow').addClass('rotate-arrow');
            $('.antique-toc .antique-block-content').addClass('hide-content');
        } else {
            $('.antique-toc .antique-block-arrow').removeClass('rotate-arrow');
            $('.antique-toc .antique-block-content').removeClass('hide-content');
        }
    }

});