(function (blocks, element, blockEditor, i18n) {
    var el = element.createElement;
    var registerBlockType = blocks.registerBlockType;
    var useBlockProps = blockEditor.useBlockProps;
    var __ = i18n.__;

    registerBlockType('antique-toc/block', {
        apiVersion: 2,
        title: __('Antique Table of Contents', 'antique-toc'),
        icon: 'list-view',
        category: 'widgets',
        edit: function () {

            return el('div', useBlockProps({}),
                    el('div', {className: 'antique-toc antique-block'},
                            el('div', {className: 'antique-toc-heading-wrap antique-block-heading-wrap'},
                                    el('span', {className: 'antique-toc-heading antique-block-heading'},
                                            __('Table of contents', 'antique-toc')),
                                    el('span', {className: 'antique-block-arrow rotate-arrow'},
                                            el('i', {className: 'fa fa-angle-down'})
                                            )
                                    )
                            )
                    );
        }
    });
})(
        window.wp.blocks,
        window.wp.element,
        window.wp.blockEditor,
        window.wp.i18n
        );

