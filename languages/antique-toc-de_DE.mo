��    &      L      |      |  g   }     �     �  �        �  	   �     �     �          
  	              %     (  %   6     \     b  �   o     �     �                   v  2     �     �     �     �     �                              $     ,     2  �  7  n   �	     ]
     m
  �   �
     �  	   �     �     �     �     �     �  
   �     �     �  *   �     (     6  �   L     �     �               ,  �  ?  *   �  '   '     O     [     b     w     |     �     �     �     �     �     �   Add a customizable table of contents to your pages so that visitors obtain an overview of your article. Antique Plugins Antique Table of Contents Apply the colors set within the Antique theme. This option only works if the Antique theme is installed and activated. Upon activation of another theme the colors specified above are used. Background color: Behaviour Block Content Block Title Border: Font color: Headings: Hide ID Large screen: Pages and posts containing the block: Reset Save changes Select the headings that are displayed in the table of contents. If you use the Antique theme, the page title cannot be displayed. Settings Show Small screen: Table of Contents Table of contents This plugin allows you to display a table of contents on your pages. When you create or edit a page, you can select the block "Antique Table of Contents" and insert it into the page. Additionally, if you are using the Antique theme, there is a "Sidebar" section in the right settings sidebar. There you can choose to display the table of contents in the sidebar of the page. Your settings have been reset. Your settings have been saved. closed content content, sidebar link location open page post sidebar title type Project-Id-Version: Antique Table of Contents
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: Simon Garbin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2023-05-26 21:02+0200
PO-Revision-Date: 2023-05-26 21:02+0200
Language: de_DE
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr__;esc_attr_e
X-Poedit-SearchPath-0: settings.php
X-Poedit-SearchPath-1: package.json
X-Poedit-SearchPath-2: antique-toc.php
X-Poedit-SearchPath-3: block/block.js
X-Poedit-SearchPath-4: block/block.json
X-Poedit-SearchPath-5: shared/menu.php
 Füge ein Inhaltsverzeichnis in deine Seiten ein, um Besuchern einen Überblick über deinen Artikel zu geben. Antique Plugins Antique Inhaltsverzeichnis Verwende die Farben, die im Antique-Theme festgelegt wurden. Diese Einstellung kann nur angewandt werden, wenn das Antique-Theme installiert und aktiviert ist. Sobald ein anderes Theme aktiviert wird, werden die oben festgelegten Farben verwendet. Hintergrundfarbe: Verhalten Block-Inhalt Block-Titel Rahmen: Schriftfarbe: Überschriften: Verstecken ID Großer Bildschirm: Seiten und Posts, die den Block enthalten: Zurücksetzen Änderungen speichern Wähle Überschriften aus, die im Inhaltsverzeichnis angezeigt werden sollen. Der Seitentitel kann im Antique-Theme nicht im Inhaltsverzeichnis angezeigt werden. Einstellungen Anzeigen Kleiner Bildschirm: Inhaltsverzeichnis Inhaltsverzeichnis Mit diesem Plugin kannst du auf deinen Seiten ein Inhaltsverzeichnis anzeigen. Wenn du eine Seite erstellst oder bearbeitest, wähle den Block "Antique Inhaltsverzeichnis" und füge ihn in deine Seite ein. Zusätzlich siehst du, falls das Antique-Theme aktiviert ist, in der rechten Seitenleiste unter den Einstellungen den Abschnitt "Seitenleiste". Dort kannst du festlegen, ob das Inhaltsverzeichnis in der Seitenleiste angezeigt werden soll. Deine Einstellungen wurden zurückgesetzt. Deine Einstellungen wurden gespeichert. geschlossen Inhalt Inhalt, Seitenleiste Link Stelle offen Seite Post Seitenleiste Titel Typ 